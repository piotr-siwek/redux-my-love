import { createStore, combineReducers, compose, applyMiddleware } from 'redux'
import thunk from 'redux-thunk'
import movies, { getMovies } from './state/moviesList'
import singleMovie, { getSingleMovie } from './state/singleMovie'

const reducers = combineReducers({
    movies,
    singleMovie
})

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose

export const store = createStore(
    reducers,
    composeEnhancers(applyMiddleware(thunk))
)

store.dispatch(getMovies('shark'))
store.dispatch(getSingleMovie('tt0307453'))

