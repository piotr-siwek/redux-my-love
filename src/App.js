import React, { Component } from 'react';
import { Provider } from 'react-redux'
import { store } from './store'
import { BrowserRouter as Router, Route } from 'react-router-dom'
import MoviesList from './Components/MoviesList'
import SearchingForm from './Components/SearchingForm'
import SingleMovieView from './Components/SingleMovieView'


class App extends Component {
    render() {
        return (
            <Provider store={store}>
                <Router>
                    <div>
                        <Route exact path="/" component={SearchingForm}/>
                        <Route exact path="/" component={MoviesList}/>
                        <Route path="/movie/:id" component={SingleMovieView}/>
                    </div>
                </Router>
            </Provider>
        );
    }
}

export default App;
