import React from 'react'
import { connect } from 'react-redux'

const SingleMovieView = props => {

    const singleMovie = props.singleMovieData

    return(
        <div>
            <h1>{singleMovie.Title} <span>({singleMovie.Year})</span></h1>
            <div>
                <div>
                    <img src={singleMovie.Poster} alt={singleMovie.Title}/>
                </div>
                <div>
                    <time>{singleMovie.Runtime}</time>
                    <p>{singleMovie.Genre}</p>
                    <p>{singleMovie.Plot}</p>
                    <p>Actors: {singleMovie.Actors}</p>
                    <p>Director: {singleMovie.Director}</p>
                    <p>Writer: {singleMovie.Writer}</p>
                    <p>Released: {singleMovie.Released}</p>
                    <p>Boxoffice: {singleMovie.BoxOffice}</p>
                </div>
            </div>
        </div>
    )
}

const mapStateToProps = state => ({
    singleMovieData: state.singleMovie.singleMovieData
})


export default connect(mapStateToProps)(SingleMovieView)