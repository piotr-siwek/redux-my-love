import React from 'react'
import { connect } from 'react-redux'
import { Link } from 'react-router-dom'
import { getSingleMovie } from "../../state/singleMovie"

const MoviesList = props => {
        return(
            <div>
                {
                    props.moviesData.map((movie, index) => (
                        <div key={index}>
                            <h2>{movie.Title}</h2>
                            <img src={movie.Poster} alt={movie.Title}/>
                            <Link to={`/movie/${movie.Title}`}>
                                <button
                                    onClick={props.getSingleMovie(movie.imdbID)}
                                >
                                    More
                                </button>
                            </Link>
                        </div>
                    ))
                }
            </div>
        )
    }


const mapStateToProps = state => ({
    moviesData: state.movies.moviesData
})

const mapDispatchToProps = dispatch => ({
    getSingleMovie: movieId => dispatch(getSingleMovie(movieId))
})


export default connect(mapStateToProps, mapDispatchToProps)(MoviesList)