const SET_MOVIES = 'movies/SET-MOVIES'

const setMovies = moviesData => ({
    type: SET_MOVIES,
    moviesData
})

export const getMovies = searchInput => (dispatch, getState) => {
    fetch(`http://www.omdbapi.com/?apikey=48586023&s=${searchInput}`)
        .then(response => response.json())
        .then(data => {
            dispatch(setMovies(data.Search))
        })
}


const initialState = {
    moviesData : []
}

export default (state = initialState, action) => {
    switch(action.type) {
        case SET_MOVIES:
            return {
                ...state,
                moviesData: action.moviesData
            }

        default:
            return state
    }
}