const SET_SINGLE_MOVIE = 'singleMovie/SET_SINGLE_MOVIE'

const setSingleMovie = singleMovieData => ({
    type: SET_SINGLE_MOVIE,
        singleMovieData
})

export const getSingleMovie = movieId => (dispatch, getState) => {
    fetch(`http://www.omdbapi.com/?apikey=48586023&i=${movieId}`)
        .then(response => response.json())
        .then(data => {
            dispatch(setSingleMovie(data))
        })
}

const initialState = {
    singleMovieData : ''
}

export default (state = initialState, action) => {
    switch(action.type) {
        case SET_SINGLE_MOVIE:
            return {
                ...state,
                singleMovieData: action.singleMovieData
            }
        default:
            return state
    }
}